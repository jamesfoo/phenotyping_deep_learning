# Phenotyping using both supervised and unsupervised algorithms in Scala and Spark
## 1. Overview
Accurate knowledge of a patient's disease state is crucial which requires knowing accurate phenotypes about patients based on their electronic health records.
There are several strategies for phenotyping, including supervised rule-based methods as well as unsupervised methods. 	
In this homework, we will implement both type of phenotyping algorithms. we will be required to implement these using Spark.

## 2. Rule based Phenotyping
Phenotyping can be done using a rule-based method. The Phenotype Knowledge Base ([PheKB](https://phekb.org) ) provides a set of rule-based methods (typically in the form of decision trees) for determining whether or not a patient fits a particular phenotype.

In this assignment, we will implement a phenotyping algorithm for type-2 diabetes based on the flowcharts below. The algorithm should

+ Take as input event data for diagnoses, medications, and lab results.
+ Return an RDD of patients with labels (*label*=1 if the patient is case, *label*=2 if the patient is control, *label*=3 otherwise).

we will implement the *Diabetes Mellitus Type 2* algorithms from PheKB. We have reduced the rules for simplicity. Thus, we should follow the simplified flowchart provided for wer homework, but can refer to http://jamia.oxfordjournals.org/content/19/2/219.long for more details if desired.

The following files in \textit{data} folder will be used as inputs:

+ **encounter_INPUT.csv**: Each line represents an envounter. The encounter ID and the patient ID (Member ID) are separate columns. \textit{Hint: sql join}
+ **encounter_dx_INPUT.csv**: Each line represents an encounter. The disgnoses (ICD9 codes) are in this file.
+ **medication_orders_INPUT.csv**: Each line represents a medication order. The name of medication is found in one of the columns on this file.
+ **lab_results_INPUT.csv**: Each line represents a lab result.

The simplified rules which we should follow for phenotyping of Diabetes Mellitus Type 2 are shown below. These rules are based off of the criteria from the PheKB phenotypes, which have been placed in the folder */phenotyping\textunderscore resources/*.

+ **Requirements for Case patients**: Figure 1 details the rules for determining whether a patient is case. Certain parts of the flowchart involve criteria that we will find in the handout folder */phekb\textunderscore criteria/*.

![Determination of Cases](expected_count_case.png "Determination of Cases")


+ **Requirements for Control patients**: Figure 2 details the rules for determining whether a patient is control. Certain parts of the flowchart involve criteria that we will find the handout folder */phekb\textunderscore criteria/*.

![Determination of Controls](expected_count_control.png "Determination of Controls")

## 3. Unsupervised phenotyping via clustering
At this point we have implemented a supervised, rule-based phenotyping algorithm. Those type of methods are great for picking out specific diseases, in our case diabetes and rheumatoid arthritis. However, they are not good for discovering new, complex phenotypes. Such phenotypes can be disease subtypes (i.e. severe hypertension, moderate hypertension, mild hypertension) or they can reflect combinations of diseases that patients may present with (e.g. a patient with hypertension and renal failure).

### 3.1 Feature Construction
Given the raw data, we need to start with feature construction. we will need to implement ETL using Spark with similar function as what we did in last homework using Pig. Given that we know which diagnoses (in the form of ICD-9 codes) each patient exhibits, and which medication each patient took, these can be used as features in a clustering model. We need to construct features using COUNT aggregation for medication and diagnostics, AVERAGE aggregation for lab test values.

### 3.2 Evaluation Metric
Purity is a metrics to measure the quality of clustering, it's defined as

\[
purity(\Omega, C) = \frac{1}{N}\sum_k \max_j |w_k \cap c_j|
\]

 where $N$ is the number of samples, $k$ is index of clusters and $j$ is index of class. $w_k$ denotes the set of samples in $k$-th cluster and $c_j$ denotes set of samples of class $j$.

### 3.3 K-Means Clustering
a. Implement $k$-means clustering for $k=3$.

b. Compare clustering for the $k=3$ case with the ground truth phenotypes that we computed for the rule-based PheKB algorithms. Specifically, for each of *case*, *control* and *unknown*, report the percentage distribution in the three clusters for the two feature construction strategies.

### 3.4 Advanced phenotyping with Non-negative Matix Factorization (NMF)
Given a feature matrix $\bf{V}$, the objective of NMF is to minimize the Euclidean distance between the original non-negative matrix $\bf{V}$ and its non-negative decomposition $\bf{W \times H}$ which can be formulated as
\[
 \underset{\bf{W} \succeq 0, \bf{H} \succeq 0 }{\text{argmin}} \quad \frac{1}{2} ||\bf{V} - \bf{W}\bf{H}||_2^2
\]

where $\bf{V} \in \mathbb{R}_{\geq 0}^{n \times m}$, $\bf{W} \in \mathbb{R}_{\geq 0}^{n \times r}$ and $\bf{H} \in \mathbb{R}_{\geq 0}^{r \times m}$. $\bf{V}$ can be considered as a dataset comprised of $n$ number of $m$-dimensional data vectors, and $r$ is generally smaller than $n$.

To obtain a $\bf{W}$ and $\bf{H}$ which will minimize the Euclidean distance between the original non-negative matrix $\bf{B}$, we use the Multiplicative Update (MU). It defines the update rule for $\bf{W}_{ij}$ and $\bf{H}_{ij}$ as

\[
\bf{W}_{ij}^{t+1}  = \bf{W}_{ij}^{t} \frac{(\bf{V} \bf{H}^{T})_{ij}}{(\bf{W}^{t} \bf{H} \bf{H}^{T})_{ij}}
\]

\[
\bf{H}_{ij}^{t+1}  = \bf{H}_{ij}^{t} \frac{(\bf{W}^{\top} \bf{V})_{ij}}{(\bf{W}^{\top} \bf{W} \bf{H}^t)_{ij}}
\]
