/**
 * @author Hang Su <hangsu@gatech.edu>.
 */

package edu.gatech.cse8803.clustering

import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD

object Metrics {
  /**
   * Given input RDD with tuples of assigned cluster id by clustering,
   * and corresponding real class. Calculate the purity of clustering.
   * Purity is defined as
   *             \fract{1}{N}\sum_K max_j |w_k \cap c_j|
   * where N is the number of samples, K is number of clusters and j
   * is index of class. w_k denotes the set of samples in k-th cluster
   * and c_j denotes set of samples of class j.
   * @param clusterAssignmentAndLabel RDD in the tuple format
   *                                  (assigned_cluster_id, class)
   * @return purity
   */
  def purity(clusterAssignmentAndLabel: RDD[(Int, Int)]): Double = {
    /**
     * TODO: Remove the placeholder and implement your code here
     */
    val N = clusterAssignmentAndLabel.count().toDouble
    val key_value = clusterAssignmentAndLabel.map(line=>(line._1, (line._1,line._2))).groupByKey()

    def max_class(line: (Int, Iterable[(Int, Int)])): Double = {
      val result = line._2.groupBy(_._2).map(_._2.map(x=>1.0).reduce(_+_)).reduce((a,b)=>Math.max(a,b)).toDouble
      result
    }

    val t = key_value.map(x=>max_class(x)).reduce(_+_)
    val tt = t/N
    tt
  }
}
