package edu.gatech.cse8803.clustering

/**
  * @author Hang Su <hangsu@gatech.edu>
  */


import breeze.linalg.{sum, DenseMatrix => BDM, DenseVector => BDV}
import breeze.linalg._
import breeze.numerics._
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.linalg.distributed.RowMatrix


object NMF {

  /**
   * Run NMF clustering 
   * @param V The original non-negative matrix 
   * @param k The number of clusters to be formed, also the number of cols in W and number of rows in H
   * @param maxIterations The maximum number of iterations to perform
   * @param convergenceTol The maximum change in error at which convergence occurs.
   * @return two matrixes W and H in RowMatrix and DenseMatrix format respectively 
   */
  def run(V: RowMatrix, k: Int, maxIterations: Int, convergenceTol: Double = 1e-4): (RowMatrix, BDM[Double]) = {

    /**
     * TODO 1: Implement your code here
     * Initialize W, H randomly 
     * Calculate the initial error (Euclidean distance between V and W * H)
     */
    val sc = V.rows.sparkContext
//    val sc = SparkContext
    val n = V.numRows()
    val m = V.numCols()

//    var H_R = new RowMatrix(V.rows.map(_ => BDV.rand[Double](k)).map(fromBreeze).cache), BDM.rand[Double](k, V.numCols().toInt)
    var W = new RowMatrix(V.rows.map(_ => BDV.rand[Double](k)).map(fromBreeze).cache)
//    W = new RowMatrix(sc.parallelize(W.rows.collect).repartition(1))
//    val V_vect = V.rows.map(line=>fromBreeze(BDV.rand[Double](k)))
//    var W = new RowMatrix(V_vect) //rowmatrix
//    var W_BDM = BDM.rand[Double](n.toInt,k)
    var H = BDM.rand[Double](k,m.toInt) //BDM

    var diff = getDenseMatrix(V)-getDenseMatrix(multiply(W,H))
    var err = 0.5*sum(diff :* diff)

//    println(err)

    /**
     * TODO 2: Implement your code here
     * Iteratively update W, H in a parallel fashion until error falls below the tolerance value 
     * The updating equations are, 
     * H = H.* W^T^V ./ (W^T^W H)
     * W = W.* VH^T^ ./ (W H H^T^)
     */

    var delta = 1.0
    var iter = 0

    while (iter < maxIterations && delta > convergenceTol) {
      val Hs = H * H.t //(k,k)

      var WVHt  = dotProd(W, multiply(V,H.t))
      var WHs = multiply(W, Hs)
      W = dotDiv(WVHt, WHs) //rowmatrix

      val Ws = computeWTV(W,W) //BDM
      H = H :* computeWTV(W,V) :/ (Ws * H + 2.0e-15)

      diff = getDenseMatrix(V)- getDenseMatrix(multiply(W,H))
      var err2 = sum(diff :* diff)*0.5

      delta = abs(err2 - err)
      err = err2

      iter = iter + 1

      W.rows.cache()
      V.rows.cache()

//      println(iter)
////      println(sum(W))
//      println(sum(H))
//      println(err2)
//      println(delta)

    }


    /** TODO: Remove the placeholder for return and replace with correct values */
//    (new RowMatrix(V.rows.map(_ => BDV.rand[Double](k)).map(fromBreeze).cache), BDM.rand[Double](k, V.numCols().toInt))
    (W, H)
  }


  /**  
  * RECOMMENDED: Implement the helper functions if you needed
  * Below are recommended helper functions for matrix manipulation
  * For the implementation of the first three helper functions (with a null return), 
  * you can refer to dotProd and dotDiv whose implementation are provided
  */
  /**
  * Note:You can find some helper functions to convert vectors and matrices
  * from breeze library to mllib library and vice versa in package.scala
  */

  /*  compute erro between W and H*/
//  private def error(V:RowMatrix, W: RowMatrix, H:BDM[Double]): Double ={
//
//    val result = 0.0
//    result
//  }


  /** compute the mutiplication of a RowMatrix and a dense matrix */
  def multiply(X: RowMatrix, d: BDM[Double]): RowMatrix = {
    val mat = fromBreeze(d)
    val result = X.multiply(mat)
    result
  }

  /** get the row matrix representation for a BDM matrix */
//  def getRowMatrix(X: BDM[Double], m:Int, n:Int,sc: SparkContext): RowMatrix={
//    val vect = X.t.toDenseVector.toArray.grouped(m).toList.map(Vectors.dense)
////    val row = sc.parallelize(vect).repartition(n)
//    val row = sc.parallelize(vect)
//    new RowMatrix(row, n, m)
//  }



 /** get the dense matrix representation for a RowMatrix */
  def getDenseMatrix(X: RowMatrix): BDM[Double] = {
    val n = X.numRows().toInt
    val m = X.numCols().toInt
    val vect = X.rows.map(x=>x.toArray).collect.flatten
//    val vect = X.rows.map(line=>toBreezeVector(line)).map(fromBreeze)
//    println(vect)
    val result = new BDM(m, n, vect).t
    result
  }

//  /** matrix multiplication of W.t and V, wherer V is freezeMatrix */
//  def computeWTV_f(W: RowMatrix, V: BDM[Double]): BDM[Double] = {
//    val W2 = getDenseMatrix(W)
////    val V2 = getDenseMatrix(V)
//    val result = W2.t * V
//    result
//  }


  /** matrix multiplication of W.t and V */
  def computeWTV(W: RowMatrix, V: RowMatrix): BDM[Double] = {
    val W2 = getDenseMatrix(W)
    val V2 = getDenseMatrix(V)
    val result = W2.t * V2
    result
  }

  /** dot product of two RowMatrixes */
  def dotProd(X: RowMatrix, Y: RowMatrix): RowMatrix = {
    val rows = X.rows.zip(Y.rows).map{case (v1: Vector, v2: Vector) =>
      toBreezeVector(v1) :* toBreezeVector(v2)
    }.map(fromBreeze)
    new RowMatrix(rows)
  }

  /** dot division of two RowMatrixes */
  def dotDiv(X: RowMatrix, Y: RowMatrix): RowMatrix = {
    val rows = X.rows.zip(Y.rows).map{case (v1: Vector, v2: Vector) =>
      toBreezeVector(v1) :/ toBreezeVector(v2).mapValues(_ + 2.0e-15)
    }.map(fromBreeze)
    new RowMatrix(rows)
  }
}
