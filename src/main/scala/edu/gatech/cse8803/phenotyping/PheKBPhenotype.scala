/**
  * @author Hang Su <hangsu@gatech.edu>,
  * @author Sungtae An <stan84@gatech.edu>,
  */

package edu.gatech.cse8803.phenotyping

import edu.gatech.cse8803.model.{Diagnostic, LabResult, Medication}
import org.apache.spark.rdd.RDD

object T2dmPhenotype {
  
  // criteria codes given
  val T1DM_DX = Set("250.01", "250.03", "250.11", "250.13", "250.21", "250.23", "250.31", "250.33", "250.41", "250.43",
      "250.51", "250.53", "250.61", "250.63", "250.71", "250.73", "250.81", "250.83", "250.91", "250.93")

  val T2DM_DX = Set("250.3", "250.32", "250.2", "250.22", "250.9", "250.92", "250.8", "250.82", "250.7", "250.72", "250.6",
      "250.62", "250.5", "250.52", "250.4", "250.42", "250.00", "250.02")

  val T1DM_MED = Set("lantus", "insulin glargine", "insulin aspart", "insulin detemir", "insulin lente", "insulin nph", "insulin reg", "insulin,ultralente")

  val T2DM_MED = Set("chlorpropamide", "diabinese", "diabanase", "diabinase", "glipizide", "glucotrol", "glucotrol xl",
      "glucatrol ", "glyburide", "micronase", "glynase", "diabetamide", "diabeta", "glimepiride", "amaryl",
      "repaglinide", "prandin", "nateglinide", "metformin", "rosiglitazone", "pioglitazone", "acarbose",
      "miglitol", "sitagliptin", "exenatide", "tolazamide", "acetohexamide", "troglitazone", "tolbutamide",
      "avandia", "actos", "actos", "glipizide")

  /**
    * Transform given data set to a RDD of patients and corresponding phenotype
    * @param medication medication RDD
    * @param labResult lab result RDD
    * @param diagnostic diagnostic code RDD
    * @return tuple in the format of (patient-ID, label). label = 1 if the patient is case, label = 2 if control, 3 otherwise
    */
  def transform(medication: RDD[Medication], labResult: RDD[LabResult], diagnostic: RDD[Diagnostic]): RDD[(String, Int)] = {
    /**
      * Remove the place holder and implement your code here.
      * Hard code the medication, lab, icd code etc. for phenotypes like example code below.
      * When testing your code, we expect your function to have no side effect,
      * i.e. do NOT read from file or write file
      *
      * You don't need to follow the example placeholder code below exactly, but do have the same return type.
      *
      * Hint: Consider case sensitivity when doing string comparisons.
      */

    val sc = medication.sparkContext

    /** Hard code the criteria */
/*    val type1_dm_dx = Set("code1", "250.03")
    val type1_dm_med = Set("med1", "insulin nph")*/

    val total_patient = diagnostic.map(line=>line.patientID).distinct()
//    println(total_patient.count())


//    implement function is_case_patient

    // step 1
    val t1DM_T = diagnostic.filter(line=> T1DM_DX.contains(line.code))
    val t1DM_T_pID = t1DM_T.map(line=>line.patientID).distinct()
    val t1DM_F_pID = total_patient.subtract(t1DM_T_pID)

    // step 2
    val t2DM_T = diagnostic.filter(line=>T2DM_DX.contains(line.code))
    val t2DM_T_pID = t2DM_T.map(line=>line.patientID).distinct()

    // step 1 + 2
    val t1DM_F_t2DM_T = t1DM_F_pID.intersection(t2DM_T_pID)

    // step 1+2+3
    val t1MED_T = medication.filter(line => T1DM_MED.contains(line.medicine.toLowerCase()))
    val t1MED_T_pID = t1MED_T.map(line=>line.patientID).distinct().intersection(t1DM_F_t2DM_T)
    val t1MED_F_pID = t1DM_F_t2DM_T.subtract(t1MED_T_pID)

    // step 1+2+3+4
    val t2MED_T = medication.filter(line=> T2DM_MED.contains(line.medicine.toLowerCase()))
    val t2MED_T_pID = t2MED_T.map(line=>line.patientID).distinct().intersection(t1MED_T_pID)
    val t2MED_F_pID = t1MED_T_pID.subtract(t2MED_T_pID)
    val all_T_set = t2MED_T_pID.collect.toSet

//    val all_T = medication.filter(line=>all_T_set.contains(line.patientID))

    val t1_T_init = t1MED_T.map(line=>(line.patientID, line.date)).reduceByKey((t1,t2)=> if (t1.before(t2)) t1 else t2)

    val t2_T_init = t2MED_T.map(line=>(line.patientID, line.date)).reduceByKey((t1,t2)=> if (t1.before(t2)) t1 else t2)

    val joined = t1_T_init.join(t2_T_init).map(x=>(x._1, if (x._2._2.before(x._2._1)) 1 else 0))

//    val last_step = joined.filter(line=> line._2==1).map(x=>x.patientID)

    val last_step = joined.filter(line=> line._2 == 1).map(line=> line._1 ).distinct()

    val is_case = t1MED_F_pID.union(t2MED_F_pID).union(last_step)
//    def init_date(med: Iterable[Medication]): Medication ={
//      val key_value = med.map(line=>(line.date, line))
//    }

//    t1_T_init.take(3).foreach(println)
//    joined.take(5).foreach(println)
//    last_step.take(5).foreach(println)


//    val t12MED_T = medication.filter(line=> t2MED_T_pID.)
//    val t1DM_F_pID = t1DM_F.map(line=>line.patientID).distinct()

//    println(t1DM_T_pID.count())
//    println(t1DM_F_pID.count())
//    println(t2DM_T_pID.count())
//    println(t1DM_F_t2DM_T.count())
//    println(t1MED_T_pID.count())
//    println(t1MED_F_pID.count())
//    println(t2MED_T_pID.count())
//    println(t2MED_F_pID.count())
//      println(all_T_set)
    /** Find CASE Patients */
//    println(last_step.count())
//    println(is_case.count())


//    val casePatients = sc.parallelize(Seq(("casePatient-one", 1), ("casePatient-two", 1), ("casePatient-three", 1)))
    val casePatients = is_case.map(line=>(line, 1))
//    casePatients.take(3).foreach(println)




    /** Find CONTROL Patients */

//      step 1: using provided "lab_filter.txt"
//    val glucose_lab = Set("HbA1c", "Hemoglobin A1c", "Fasting Glucose", "Fasting blood glucose", "fasting plasma glucose",
//                        "Glucose", "glucose", "Glucose, Serum")
//    val glucose_lab2 = Set("hba1c", "hemoglobin a1c", "fasting glucose", "fasting blood glucose", "fasting plasma glucose",
//                        "glucose", "glucose, serum")
//    val glucose_T_pID = labResult.filter(line=>glucose_lab.contains(line.testName)).map(line=>line.patientID).distinct()
//    val glucose_T_pID2 = labResult.filter(line=>glucose_lab2.contains(line.testName.toLowerCase())).map(line=>line.patientID).distinct()
    val glucose_T_pID = labResult.filter(line=>line.testName.toLowerCase().contains("glucose")).map(line=>line.patientID).distinct()

    def is_abnormal(line:LabResult):Int = {
      var is_abnorm = 0
      if (line.testName.toLowerCase() == "hba1c" && line.value>=6.0) is_abnorm=1
      else if (line.testName.toLowerCase() == "hemoglobin a1c" && line.value>=6.0) is_abnorm=1
      else if (line.testName.toLowerCase() == "fasting glucose" && line.value >= 110.0) is_abnorm=1
      else if (line.testName.toLowerCase() == "fasting blood glucose" && line.value>= 110.0) is_abnorm=1
      else if (line.testName.toLowerCase() == "fasting plasma glucose" && line.value>= 110.0) is_abnorm=1
      else if (line.testName.toLowerCase() == "glucose" && line.value>110.0) is_abnorm=1
      else if (line.testName.toLowerCase() == "glucose, serum" && line.value>110.0) is_abnorm=1

      is_abnorm
    }

    // step 2
//    val tot_lab_patient = labResult.map(line=>line.patientID).distinct()

    val abnormal_pID = labResult.filter(line=> is_abnormal(line)==1).map(line=>line.patientID).distinct()
//    println(abnormal_pID.count())

    val normal_pID = total_patient.subtract(abnormal_pID)
//    println(normal_pID.count())
//    val normal_pID = tot_lab_patient.subtract(abnormal_pID)

    val glu_T_abnom_F = glucose_T_pID.intersection(normal_pID)


    val DM_RELAT_DX = Set("790.21","790.22", "790.2", "790.29", "648.81","648.82", "648.83", "648.84","648", "648.01", "648.02",
                          "648.03", "648.04", "791.5", "277.7", "V77.1", "256.4", "250.*")

//    val diag_melitus_T = diagnostic.filter(line=>DM_RELAT_DX.contains(line.code)).map(line=>line.patientID).distinct()
    val diag_melitus_T = diagnostic.filter(line => DM_RELAT_DX.map(y => line.code.matches(y)).reduce(_ || _)).map(line=>line.patientID).distinct()


    val diag_melitus_F = total_patient.subtract(diag_melitus_T)
    val is_control_pID = glu_T_abnom_F.intersection(diag_melitus_F)

//    println(diag_melitus_T.count())
//
//    println(is_control_pID.count())

//    println(tot_lab_patient.count())
//    println(glu_T_abnom_F.count())
//    val tot_lab = labResult.map(line=>line.patientID).distinct()

//    println(tot_lab.count())

//    println(glucose_T_pID.count())
//    println(glucose_T_pID2.count())
//    println(glucose_T_pID.count())

//    val controlPatients = sc.parallelize(Seq(("controlPatients-one", 2), ("controlPatients-two", 2), ("controlPatients-three", 2)))
    val controlPatients = is_control_pID.map(line=> (line, 2))

    /** Find OTHER Patients */

    val case_control_pID = is_control_pID.union(is_case)
    val other_pID = total_patient.subtract(case_control_pID)
    val others = other_pID.map(line=>(line,3))

//    val others = sc.parallelize(Seq(("others-one", 3), ("others-two", 3), ("others-three", 3)))

    /** Once you find patients for each group, make them as a single RDD[(String, Int)] */
    val phenotypeLabel = sc.union(casePatients, controlPatients, others)

    /** Return */
    phenotypeLabel
  }

}
