/**
 * @author Hang Su <hangsu@gatech.edu>.
 */

package edu.gatech.cse8803.main

import java.text.SimpleDateFormat

import edu.gatech.cse8803.clustering.{Metrics, NMF}
import edu.gatech.cse8803.features.FeatureConstruction
import edu.gatech.cse8803.ioutils.CSVUtils
import edu.gatech.cse8803.model.{Diagnostic, LabResult, Medication}
import edu.gatech.cse8803.phenotyping.T2dmPhenotype
import org.apache.spark.SparkContext._
import org.apache.spark.mllib.clustering.{GaussianMixture, KMeans, StreamingKMeans, StreamingKMeansModel}
import org.apache.spark.mllib.linalg.{DenseMatrix, Matrices, Vector, Vectors}
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source

object Main {
  def main(args: Array[String]) {
    import org.apache.log4j.Logger
    import org.apache.log4j.Level

    Logger.getLogger("org").setLevel(Level.WARN)
    Logger.getLogger("akka").setLevel(Level.WARN)

    val sc = createContext
    val sqlContext = new SQLContext(sc)

    /** initialize loading of data */
    val (medication, labResult, diagnostic) = loadRddRawData(sqlContext)
    val (candidateMedication, candidateLab, candidateDiagnostic) = loadLocalRawData

    /** conduct phenotyping */
    val phenotypeLabel = T2dmPhenotype.transform(medication, labResult, diagnostic)

    /** feature construction with all features */
    val featureTuples = sc.union(
      FeatureConstruction.constructDiagnosticFeatureTuple(diagnostic),
      FeatureConstruction.constructLabFeatureTuple(labResult),
      FeatureConstruction.constructMedicationFeatureTuple(medication)
    )

    val rawFeatures = FeatureConstruction.construct(sc, featureTuples)

    val (kMeansPurity, gaussianMixturePurity, streamKmeansPurity, nmfPurity) = testClustering(phenotypeLabel, rawFeatures)
    println(f"[All feature] purity of kMeans is: $kMeansPurity%.5f")
    println(f"[All feature] purity of GMM is: $gaussianMixturePurity%.5f")
    println(f"[All feature] purity of StreamingKMeans is: $streamKmeansPurity%.5f")
    println(f"[All feature] purity of NMF is: $nmfPurity%.5f")

    /** feature construction with filtered features */
    val filteredFeatureTuples = sc.union(
      FeatureConstruction.constructDiagnosticFeatureTuple(diagnostic, candidateDiagnostic),
      FeatureConstruction.constructLabFeatureTuple(labResult, candidateLab),
      FeatureConstruction.constructMedicationFeatureTuple(medication, candidateMedication)
    )

    val filteredRawFeatures = FeatureConstruction.construct(sc, filteredFeatureTuples)

    val (kMeansPurity2, gaussianMixturePurity2, streamKmeansPurity2, nmfPurity2) = testClustering(phenotypeLabel, filteredRawFeatures)
    println(f"[Filtered feature] purity of kMeans is: $kMeansPurity2%.5f")
    println(f"[Filtered feature] purity of GMM is: $gaussianMixturePurity2%.5f")
    println(f"[Filtered feature] purity of StreamingKMeans is: $streamKmeansPurity2%.5f")
    println(f"[Filtered feature] purity of NMF is: $nmfPurity2%.5f")
    sc.stop 
  }

  def testClustering(phenotypeLabel: RDD[(String, Int)], rawFeatures:RDD[(String, Vector)]): (Double, Double, Double, Double) = {
    import org.apache.spark.mllib.linalg.Matrix
    import org.apache.spark.mllib.linalg.distributed.RowMatrix

    /** scale features */
    val scaler = new StandardScaler(withMean = true, withStd = true).fit(rawFeatures.map(_._2))
    val features = rawFeatures.map({ case (patientID, featureVector) => (patientID, scaler.transform(Vectors.dense(featureVector.toArray)))})
    val rawFeatureVectors = features.map(_._2).cache()

    /** reduce dimension */
    val mat: RowMatrix = new RowMatrix(rawFeatureVectors)
    val pc: Matrix = mat.computePrincipalComponents(10) // Principal components are stored in a local dense matrix.
    val featureVectors = mat.multiply(pc).rows.cache()

    val densePc = Matrices.dense(pc.numRows, pc.numCols, pc.toArray).asInstanceOf[DenseMatrix]
    /** transform a feature into its reduced dimension representation */
    def transform(feature: Vector): Vector = {
      Vectors.dense(Matrices.dense(1, feature.size, feature.toArray).multiply(densePc).toArray)
    }

    /** TODO: K Means Clustering using spark mllib
      *  Train a k means model using the variabe featureVectors as input
      *  Set maxIterations =20 and seed as 8803L
      *  Assign each feature vector to a cluster(predicted Class)
      *  Obtain an RDD[(Int, Int)] of the form (cluster number, RealClass)
      *  Find Purity using that RDD as an input to Metrics.purity
      *  Remove the placeholder below after your implementation
      **/
//    val kMeansPurity = 0.0

    val k_cluster= 3
    val maxIterations = 20
    val seed = 8803L

    val kmeans = new KMeans().setK(k_cluster).setMaxIterations(maxIterations).setSeed(seed)
    val model = kmeans.run(featureVectors)
    val pred = model.predict(featureVectors).zipWithIndex.map(line=>(line._2, line._1)) //(index, label)
//    val pred2 = model.predict(featureVectors) // label
//    pred.take(5).foreach(println)


    val raw_label = rawFeatures.join(phenotypeLabel).map(line=>line._2).cache() //(feature, label)
    val true_label = raw_label.map(_._2).zipWithIndex.map(line=>(line._2, line._1)) // (index, patientID)
//    raw_label.take(5).foreach(println)
//    true_label.take(5).foreach(println)

    val patient_pred = pred.join(true_label).map(line=>line._2) //(pred, true)
//    patient_pred.take(5).foreach(println)

//    var table = patient_pred.filter(line=>line._2==1).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(table.toString)
//
//    table = patient_pred.filter(line=>line._2==2).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(table.toString)
//
//    table = patient_pred.filter(line=>line._2==3).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(table.toString)

//    val raw = rawFeatures.map(_._1).zipWithIndex.map(line=> (line._2, line._1)) //(index, patientID)
//    val patient_pred = raw.join(pred)
//    val patient_pred2 = patient_pred.map(line=>line._2)

//    patient_pred.take(5).foreach(println)
//    patient_pred2.take(5).foreach(println)

//    rawFeatures.take(5).foreach(println)
//    println(".........")
//    phenotypeLabel.take(5).foreach(println)
//    println(".........")
//    pred2.take(5).foreach(println)



    val kMeansPurity = Metrics.purity(patient_pred)



    /** TODO: GMMM Clustering using spark mllib
      *  Train a Gaussian Mixture model using the variabe featureVectors as input
      *  Set maxIterations =20 and seed as 8803L
      *  Assign each feature vector to a cluster(predicted Class)
      *  Obtain an RDD[(Int, Int)] of the form (cluster number, RealClass)
      *  Find Purity using that RDD as an input to Metrics.purity
      *  Remove the placeholder below after your implementation
      **/

    val GMM = new GaussianMixture().setK(k_cluster).setMaxIterations(maxIterations).setSeed(seed)

    val GMM_model = GMM.run(featureVectors)
    val GMM_pred = GMM_model.predict(featureVectors).zipWithIndex.map(line=>(line._2, line._1)) //(id, pred)

    val GMM_pred_true = GMM_pred.join(true_label).map(line=>line._2) //(pred, true)
    //    patient_pred.take(5).foreach(println)

//    var GMM_table = GMM_pred_true.filter(line=>line._2==1).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(GMM_table.toString)
//
//    GMM_table = GMM_pred_true.filter(line=>line._2==2).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(GMM_table.toString)
//
//    GMM_table = GMM_pred_true.filter(line=>line._2==3).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(GMM_table.toString)

    val gaussianMixturePurity = Metrics.purity(GMM_pred_true)

    /** TODO: StreamingKMeans Clustering using spark mllib
      *  Train a StreamingKMeans model using the variabe featureVectors as input
      *  Set the number of cluster K = 3 and DecayFactor = 1.0 and weight as 0.0
      *  please pay attention to the input type
      *  Assign each feature vector to a cluster(predicted Class)
      *  Obtain an RDD[(Int, Int)] of the form (cluster number, RealClass)
      *  Find Purity using that RDD as an input to Metrics.purity
      *  Remove the placeholder below after your implementation
      **/
    val decayFactor = 1.0
    val weight = 0.0
    val dim = 165
//    val tunit= "second"
//    val centers = Array(0,0,0,0)
//    val weights = Array(0,0,0,0)
//    val conf = new SparkConf().setMaster("local[*]").setAppName("skm")
//    val spc = new SparkContext(conf)
//    val ssc = new StreamingContext(spc, Seconds(10))
//    val queue = new scala.collection.mutable.Queue[RDD[Vector]]
//    queue.enqueue(featureVectors)
//    val dstream = ssc.queueStream(queue,false)
//
//    val SKM = new StreamingKMeans().setK(k).setDecayFactor(decayFactor).setRandomCenters(dim, weight)
//    SKM.trainOn(dstream)



//    val SKMeans = new StreamingKMeans().setK(k_cluster).setDecayFactor(decayFactor).setRandomCenters(dim, weight).latestModel()
//    val SKMmeans_model = SKMeans.update(featureVectors, decayFactor, "batches")
//////    val SKMeans_model = SKMeans.update(featureVectors, decayFactor, tunit)
//    val SKMeans_pred = SKMeans.predict(featureVectors)
//    SKMeans_pred.take(5).foreach(println)

//    val SKM_pred_true = SKMeans_pred.join(true_label).map(line=>line._2) //(pred,true)
    val streamKmeansPurity = 0.0



    /** NMF */
    val rawFeaturesNonnegative = rawFeatures.map({ case (patientID, f)=> Vectors.dense(f.toArray.map(v=>Math.abs(v)))})
    val (w, _) = NMF.run(new RowMatrix(rawFeaturesNonnegative), k_cluster, 100)
    // for each row (patient) in W matrix, the index with the max value should be assigned as its cluster type
    val assignments = w.rows.map(_.toArray.zipWithIndex.maxBy(_._1)._2)
    // zip patientIDs with their corresponding cluster assignments
    // Note that map doesn't change the order of rows
    val assignmentsWithPatientIds=features.map({case (patientId,f)=>patientId}).zip(assignments)
    // join your cluster assignments and phenotypeLabel on the patientID and obtain a RDD[(Int,Int)]
    // which is a RDD of (clusterNumber, phenotypeLabel) pairs
    val nmfClusterAssignmentAndLabel = assignmentsWithPatientIds.join(phenotypeLabel).map({case (patientID,value)=>value})
    // Obtain purity value
    val nmfPurity = Metrics.purity(nmfClusterAssignmentAndLabel)


//    var NMF_table = nmfClusterAssignmentAndLabel.filter(line=>line._2==1).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(NMF_table.toString)
//
//    NMF_table = nmfClusterAssignmentAndLabel.filter(line=>line._2==2).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(NMF_table.toString)
//
//    NMF_table = nmfClusterAssignmentAndLabel.filter(line=>line._2==3).map(x=>x._1).countByValue()
//    println("talbe-------------------")
//    println(NMF_table.toString)





    (kMeansPurity, gaussianMixturePurity, streamKmeansPurity, nmfPurity)
  }

  /**
   * load the sets of string for filtering of medication
   * lab result and diagnostics
    *
    * @return
   */
  def loadLocalRawData: (Set[String], Set[String], Set[String]) = {
    val candidateMedication = Source.fromFile("data/med_filter.txt").getLines().map(_.toLowerCase).toSet[String]
    val candidateLab = Source.fromFile("data/lab_filter.txt").getLines().map(_.toLowerCase).toSet[String]
    val candidateDiagnostic = Source.fromFile("data/icd9_filter.txt").getLines().map(_.toLowerCase).toSet[String]
    (candidateMedication, candidateLab, candidateDiagnostic)
  }

  def loadRddRawData(sqlContext: SQLContext): (RDD[Medication], RDD[LabResult], RDD[Diagnostic]) = {
    /** You may need to use this date format. */
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX")

    /** load data using Spark SQL into three RDDs and return them
      * Hint: You can utilize edu.gatech.cse8803.ioutils.CSVUtils and SQLContext.
      *
      * Notes:Refer to model/models.scala for the shape of Medication, LabResult, Diagnostic data type.
      *       Be careful when you deal with String and numbers in String type.
      *       Ignore lab results with missing (empty or NaN) values when these are read in.
      *       For dates, use Date_Resulted for labResults and Order_Date for medication.
      * */

    /** TODO: implement your own code here and remove existing placeholder code below */
/*    val medication: RDD[Medication] =  sqlContext.sparkContext.emptyRDD */
/*    val labResult: RDD[LabResult] =  sqlContext.sparkContext.emptyRDD*/
/*    val diagnostic: RDD[Diagnostic] =  sqlContext.sparkContext.emptyRDD*/

    val med_DF= CSVUtils.loadCSVAsTable(sqlContext, "data/medication_orders_INPUT.csv", "med").select("Member_ID", "Order_Date", "Drug_Name")
    val medication: RDD[Medication] = med_DF.map(line => Medication(line.getString(0), dateFormat.parse(line.getString(1)), line.getString(2)))

    val lab_DF = CSVUtils.loadCSVAsTable(sqlContext, "data/lab_results_INPUT.csv", "lab").select("Member_ID", "Date_Resulted", "Result_Name", "Numeric_Result")
    val lab_DF2 = lab_DF.withColumn("Numeric_Result2", lab_DF("Numeric_Result").cast("double"))
    val lab_DF3 = lab_DF2.select("Member_ID", "Date_Resulted", "Result_Name", "Numeric_Result2").withColumnRenamed("Numeric_Result2", "Numeric_Result").na.drop()
    val labResult: RDD[LabResult] = lab_DF3.map(line => LabResult(line.getString(0), dateFormat.parse(line.getString(1)), line.getString(2), line.getDouble(3)))


    val dia_DF = CSVUtils.loadCSVAsTable(sqlContext, "data/encounter_dx_INPUT.csv", "dia")
    val encounter_DF = CSVUtils.loadCSVAsTable(sqlContext, "data/encounter_INPUT.csv", "encounter").select("Encounter_ID", "Member_ID", "Encounter_DateTime")
    val encounter_join = dia_DF.join(encounter_DF, dia_DF("Encounter_ID").equalTo(encounter_DF("Encounter_ID"))).select("Member_ID", "Encounter_DateTime", "code").na.drop()
    val diagnostic: RDD[Diagnostic] = encounter_join.map(line => Diagnostic(line.getString(0), dateFormat.parse(line.getString(1)), line.getString(2)))



    //test medication
//    println(medication.count())
//    println(lab_DF3.printSchema())
//    println(labResult.count())
//    println(diagnostic.count())

    (medication, labResult, diagnostic)
  }

  def createContext(appName: String, masterUrl: String): SparkContext = {
    val conf = new SparkConf().setAppName(appName).setMaster(masterUrl)
    new SparkContext(conf)
  }

  def createContext(appName: String): SparkContext = createContext(appName, "local")

  def createContext: SparkContext = createContext("CSE 8803 Homework Two Application", "local")
}
